<?php
    session_start();
    require("Accessories/database.php");
       
    if(isset($_REQUEST['uid'])) {
        $id = $_REQUEST['uid'];
    } 

    $select = "SELECT * FROM `users` WHERE `id` = ".$id;
    $user = mysqli_query($conn ,$select);
    $user_data = mysqli_fetch_assoc($user);

    include 'Accessories/server.php';
?>
<html>
    <head>
        <title>Update your details</title>
        <?php include 'Accessories/head.php'; ?>
    </head>
    <body style="align-content: center;">
        <a href="index.php">Go to Dashboard</a>
        <h2><center><b>Update your details</b></center></h2>

        <?php
            if(isset($_SESSION['user_image']) && $_SESSION['user_image'] == "Image deleted") {

                $session = $_SESSION['user_image'];
                unset($_SESSION['user_image']);
                session_destroy();
                ?>
                    <span class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong><?php echo $session ;?> successfully</strong> 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </span>
                <?php
            }
        ?>

        <div style="margin-left: 40%;">
            <img src="Accessories/images/<?php echo $user_data['image']; ?>" style="width: 100px; height: 100px;">
        </div><br>
        <div style="margin-left: 50%; margin-top: -100px;">
            <?php
                if($user_data['image'] !== "user.png") {
                    ?>
                        <form action="Accessories/server.php?uid=<?php echo $id; ?>" method="POST">
                            <button type="submit" name="delete_img" value="1" class="btn btn-outline-danger col-sm-2">
                                Delete Image
                            </button>
                        </form>
                    <?php
                } else {
                    ?>
                        <p style="width: 150px; height: 50px;">Upload your gorgeous image to make a profile pic.</p>
                    <?php
                }
            ?>
        </div><br>
        
        <center>    
            <div class="col-md-6 mt-4">
                <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?uid=<?php echo $_REQUEST['uid']; ?>" enctype="multipart/form-data">

                    <?php include 'Accessories/form.php'; ?>  
                    
                    <input type="hidden" name="type" value="EDIT">
                    <input type="hidden" name="uid" value="<?php echo $id; ?>">
                    <button type="submit" class="btn btn-primary col-md-12">Update</button>
                </form>
            </div>
        </center>
    </body>
</html>