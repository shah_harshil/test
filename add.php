<?php
    include 'Accessories/server.php';
?>

<html>
    <head>
        <title>Registeration form</title>
        <?php include 'Accessories/head.php'; ?>
    </head>
    <body style="align-content: center;">
        <a href="index.php">Go to Dashboard</a>
        <h2><center><b>Registeration form</b></center></h2>
        
        <center>
            <div class="col-md-6 mt-4">
                <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">

                    <?php include 'Accessories/form.php'; ?>  
                    
                    <input type="hidden" name="type" value="ADD">
                    <button type="submit" class="btn btn-primary col-md-12">Submit</button>
                </form>
            </div>
        </center>
    </body>
</html>