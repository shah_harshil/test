<div class="md-3">
    <label for="image" class="form-label"><b>Upload Image: </b></label>
    <input type="file" class="form-control" id="image" name="image">
    <span style="color: red"><?php echo $error_image; ?></span>
</div>  <input type="hidden" name="type" value="ADD">
<br>

<div class="md-3">
    <label for="first_name" class="form-label"><b>First Name: </b></label>
    <input type="text" class="form-control" id="first_name" name="first_name" 
    value="<?php echo (isset($_POST['first_name'])) ? $_POST['first_name'] : $user_data['first_name'];?>">
    <span style="color:red"><?php echo $error_fname; ?></span>
</div>
<br>

<div class="md-3">
    <label for="last_name" class="form-label"><b>Last Name: </b></label>
    <input type="text" class="form-control" id="last_name" name="last_name" 
    value="<?php echo (isset($_POST['last_name'])) ? $_POST['last_name'] : $user_data['last_name'];?>">
    <span style="color:red"><?php echo $error_lname; ?></span>
</div>
<br>

<div class="md-3">
    <label for="country" class="form-label"><b>Country: </b></label>

    <select name="country" id="country" class="form-control">
        <option value="">Select your native country</option>
        <?php
            $country_all = array("America", "Australia", "S. Africa", "China", "England", "India", "Russia");

            foreach ($country_all as $key => $value) {
                ?>
                    <option value="<?php echo $value; ?>" 
                    <?php 
                        if(isset($_POST['country'])) {
                            if($_POST['country'] == $value) 
                                echo "selected"; 
                        } elseif ($user_data['country'] == $value) {
                            echo "selected";
                        } 
                    ?>>
                        <?php echo $value; ?>
                    </option>
                <?php
            }
        ?>
    </select>
    <span style="color:red"><?php echo $error_country; ?></span>
</div>
<br>

<div class="md-3">
    <label for="email" class="form-label"><b>Email: </b></label>
    <input type="text" class="form-control" id="email" name="email" 
    value="<?php echo (isset($_POST['email'])) ? $_POST['email'] : $user_data['email'];?>">

    <span style="color:red"><?php echo $error_email; ?></span>
</div>
<br>

<div class="md-3">
    <label for="gender" class="form-label"><b>Gender: </b></label>

    <?php

        $Gender_all = array("Male", "Female", "Others");
        
        foreach ($Gender_all as $key => $value) 
        {
            ?>
            <input type="radio" name="gender" value="<?php echo $value; ?>" 
                <?php 
                    if(isset($user_data['gender']) && $user_data['gender'] == $value) 
                        echo "checked"; 
                    else if($_REQUEST['gender'] == $value)
                        echo "checked";
                ?>> <?php echo $value; ?>
            <?php
        }
    ?>

    <span style="color:red"><?php echo $error_gender; ?></span>
</div>
<br>

<div class="md-3">
    <label for="dob" class="form-label"><b>Date of Birth: </b></label>
    <input type="date" name="dob" id="dob"
    value="<?php echo (isset($_POST['dob'])) ? $_POST['dob'] : $user_data['dob'];?>">
    <span style="color:red"><?php echo $error_dob; ?></span>
</div>
<br>

<div class="md-3">
    <label for="description" class="form-label"><b>Something about you: </b></label>
    <textarea name="description" id="description" cols="65" rows="5"><?php echo (isset($_POST['description'])) ? $_POST['description'] : $user_data['description'];?></textarea>
    <span style="color:red"><?php echo $error_description; ?></span>
</div>
<br>

<div class="md-3">
    <label for="phone" class="form-label"><b>Contact no.: </b></label>
    <input type="number" name="phone" id="phone"
    value="<?php echo (isset($_POST['phone'])) ? $_POST['phone'] : $user_data['phone'];?>">
    <span style="color:red"><?php echo $error_phone; ?></span>
</div>
<br>

<div class="md-3">
    <label for="hobbies" class="form-label"><b>Hobbies: </b></label>
    <?php
        $user_hobbies = explode(',', $user_data['hobbies']);

        $Hobbies_all = array("Sports", "Movies", "Singing", "Reading", "Traveling");

        foreach ($Hobbies_all as $key => $value) {
            ?>
                <input type="checkbox" name="hobbies[]" value="<?php echo $value; ?>" 
                <?php 
                    if(in_array($value, $user_hobbies))  {
                        echo "checked";
                    } else if (is_array($_REQUEST['hobbies'])) {
                        if(in_array($value, $_REQUEST['hobbies'])) {
                            echo "checked";
                        }
                    }
                         
                ?>> <?php echo $value; ?>
            <?php
        }
    ?>
</div>
<br>