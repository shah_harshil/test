<?php
session_start();
require 'database.php';

$is_data_valid = true;

#Validating user input data
if (isset($_POST['type'])) {

    $first_name = $_REQUEST['first_name'];
    $last_name = $_REQUEST['last_name'];
    $country = $_REQUEST['country'];
    $email = $_REQUEST['email'];
    $gender = $_REQUEST['gender'];
    $dob = $_REQUEST['dob'];
    $description = $_REQUEST['description'];
    $phone = $_REQUEST['phone'];
    $Hobbies = $_REQUEST['hobbies'];
    $h = @implode(',', $_REQUEST['hobbies']);

    if(!empty($_FILES['image']['name'])) {

        $path_parts = pathinfo($_FILES['image']['name']);
        $ext = $path_parts['extension'];

        if($ext !== "jpeg" && $ext !== "jpg" && $ext !== "png") {
            $error_image = "Image file must have .jpeg, .jpg or .png extension only";
            $is_data_valid = false;
        } else {
            $image_name = $_FILES['image']['name'];
            $image_path = __DIR__.'/images/'.$image_name;
            move_uploaded_file($_FILES['image']['tmp_name'], $image_path);
        }
    } elseif($_POST['type'] == 'EDIT') {
        $image_name = $user_data['image'];
    } else {
        $image_name = "user.png";
    }
    
    if (empty($first_name)) {
        $error_fname = 'First Name field is required!';
        $is_data_valid = false;
    }

    if (empty($last_name)) {
        $error_lname = 'Last Name field is required!';
        $is_data_valid = false;
    }

    if (empty($country)) {
        $error_country = 'Native Country field is required!';
        $is_data_valid = false;
    }

    if (empty($email)) {
        $error_email = 'Email field is required!';
        $is_data_valid = false;
    }
    
    if (empty($gender)) {
        $error_gender = 'Gender field is required!';
        $is_data_valid = false;
    }
    
    if (empty($dob)) {
        $error_dob = 'DoB field is required!';
        $is_data_valid = false;
    }
    
    if (empty($description)) {
        $error_description = 'Description field is required!';
        $is_data_valid = false;
    }
    
    if (empty($phone)) {
        $error_phone = 'Contact number field is required!';
        $is_data_valid = false;
    } else if(strlen($phone) > 14 || strlen($phone) < 9) {
        $error_phone = 'Contact number field is in improper format';
        $is_data_valid = false;
    }

    $base_query = "SET `image` = '".$image_name."', `first_name` = '".$first_name."', `last_name` = '".$last_name."', `email` = '".$email."', `gender` = '".$gender."', `dob` = '".$dob."', `description` = '".$description."', `phone` = '".$phone."', `hobbies` = '".$h."', `country` = '".$country."'";
}

# START: Add user module
if (isset($_POST['type']) && $_POST['type'] == 'ADD') {

    // Insert user to database
    if ($is_data_valid) {

        $insert = "INSERT INTO `users` ".$base_query;

        $run = mysqli_query($conn, $insert); 

        if($run) {
            $_SESSION['user'] = "Record added";
            header('Location: index.php');
        } else {
            echo mysqli_error($conn);
        }
    }
}
# END: Add user module


# START: Edit user module
else if (isset($_POST['type']) && $_POST['type'] == 'EDIT') {

    if ($is_data_valid) {
        
        $id = $_REQUEST['uid'];

        $update = "UPDATE `users` ".$base_query." WHERE `id` = '".$id."'";  

        $run_update = mysqli_query($conn, $update);

        if($run_update) {
            $_SESSION['user'] = "Record updated";
            header('Location: index.php');
        } else {
            echo mysqli_error($conn);
        }
    }
}
# END: Edit user module

#START: DELETE user module
else if ($_GET['type'] == "delete") {

    $id = $_REQUEST['uid'];

    $delete = "DELETE FROM `users` WHERE `id` = ".$id;
    $run = mysqli_query($conn, $delete);

    if($run) {
        $_SESSION['user'] = "Record deleted";
        header('Location: /USER_CRUD/index.php');
    } else {
        echo mysqli_error($conn);
    }
}
#END: DELETE user module

#START: DELETE user image module
else if (isset($_POST['delete_img'])) {

    $id = $_REQUEST['uid'];

    $delete_img = "UPDATE `users` SET `image` = 'user.png' WHERE `id` = '".$id."' LIMIT 2";
    $run_delete_img = mysqli_query($conn, $delete_img);

    if($run_delete_img) {
        $_SESSION['user_image'] = "Image deleted";
        header('Location: /USER_CRUD/edit.php?uid='.$id);
    } else {
        echo mysqli_error($conn);
    }
}
#END: DELETE user image module

#START: RESET FILTER module
if ($_GET['type'] == "reset_filter") {
    header('Location: /USER_CRUD/index.php');
}
#END: RESET FILTER module

#START: FILTER user module
if ($_GET['type'] == "search") {

    $search_fname = $_GET['search_fname'];
    $search_lname = $_GET['search_lname'];
    $search_email = $_GET['search_email'];
    $search_phone = $_GET['search_phone'];
    $search_gender = $_GET['search_gender'];

    if ($_GET['date_from'] != "") {
        $date_from = date_format(date_create($_GET['date_from']),"Y-m-d H:i:s");
    }
    
    $date_to = date_format(date_create($_GET['date_to']),"Y-m-d 23:59:59");
    
    $select = "SELECT * FROM `users` WHERE ";
    $flag = 0;

    if ($search_fname != "") {
        $select = $select."`first_name` LIKE '%".$search_fname."%' ";
        $flag++;
    }

    if ($search_lname != "") {
        if ($flag > 0) {
            $and = "AND ";
        } else {
            $and = "";
        }
        $select = $select.$and." `last_name` LIKE '%".$search_lname."%' ";
        $flag++;
    }

    if ($search_email != "") {
        if ($flag > 0) {
            $and = "AND ";
        } else {
            $and = "";
        }
        $select = $select.$and." `email` LIKE '%".$search_email."%' ";
        $flag++;
    }

    if ($search_phone != "") {
        if ($flag > 0) {
            $and = "AND ";
        } else {
            $and = "";
        }
        $select = $select.$and." `phone` LIKE '%$search_phone%' ";
        $flag++;
    }

    if ($search_gender != "") {
        if ($flag > 0) {
            $and = "AND ";
        } else {
            $and = "";
        }
        $select = $select.$and." `gender` = '".$search_gender."'";
        $flag++;
    }

    if ($date_from != "" && $date_to != "") {
        if ($flag > 0) {
            $and = "AND ";
        } else {
            $and = "";
        }
        $select = $select.$and." `created_at` BETWEEN '".$date_from."' AND '".$date_to."'";
        $flag++;

    } elseif ($date_from == "" && $date_to != "") {  
        if ($flag > 0) {
            $and = "AND ";
        } else {
            $and = "";
        }
        $select = $select.$and." `created_at` BETWEEN '2021-08-02 17:22:58' AND '".$date_to."'";

    }
    
    $run = mysqli_query($conn ,$select);

} else {

    $start = 0;
    if (isset($_REQUEST['start']) && $_REQUEST['start'] != "" && $_REQUEST['start'] < 6 && $_REQUEST['start'] > 0) {
        $start = $_REQUEST['start'];
    }

    $num = 5;
    if (isset($_REQUEST['num']) && $_REQUEST['num'] != "") {
        $num = $_REQUEST['num'];
    } elseif ($_REQUEST['page_length']) {
        $num = $_REQUEST['page_length'];
    } 

    if(isset($_REQUEST['sort'])) {

        $sort_var = $_REQUEST['sort'];
        $orientation = $_REQUEST['type'];

        $select = "SELECT * FROM `users` ORDER BY `".$sort_var."` ".$orientation." LIMIT $start, $num";
        $run = mysqli_query($conn, $select);

    } else {
        $select = "SELECT * FROM `users` LIMIT $start, $num";
        $run = mysqli_query($conn ,$select);
    }
}
#END: FILTER user module
