<?php 
//code by anand

    session_start(); 

    require("Accessories/database.php");
    require("Accessories/server.php");
?>
<html>
    <head>
        <?php include 'Accessories/head.php'; ?>
    </head>
    <body>
        <h1><center><b>Dashboard123</b></center></h1>

        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">

            <p><b><center style="color: red;">Filter the records you want to fetch</center></b></p>

            <input type="search" name="search_fname" id="search_fname" placeholder="First Name" class="form-control col-sm-2" style="margin-left: 110px;" value="<?php echo $_REQUEST['search_fname']; ?>">

            <input type="search" name="search_lname" id="search_lname" placeholder="Last Name" class="form-control col-sm-2" style="margin-left: 400px; margin-top: -38px;" value="<?php echo $_REQUEST['search_lname']; ?>">

            <input type="search" name="search_email" id="search_email" placeholder="Email" class="form-control col-sm-2" style="margin-left: 700px; margin-top: -38px;" value="<?php echo $_REQUEST['search_email']; ?>">

            <input type="search" name="search_phone" id="search_phone" placeholder="Mobile number" class="form-control col-sm-2" style="margin-left: 970px; margin-top: -38px;" value="<?php echo $_REQUEST['search_phone']; ?>"><br>

            <div style="margin-left: 210px;">From: </div><input type="date" name="date_from" id="date_from" class="form-control col-sm-2" style="margin-left: 270px; margin-top: -34px;" value="<?php if ($_REQUEST['date_from'] != "") echo date("Y-m-d", strtotime($_REQUEST['date_from'])); ?>">

            <div style="margin-left: 520px; margin-top: -34px;">To: </div><input type="date" name="date_to" id="date_to" class="form-control col-sm-2" style="margin-left: 560px; margin-top: -30px;" value="<?php if ($_REQUEST['date_to'] != "") echo date("Y-m-d", strtotime($_REQUEST['date_to'])); ?>">

            <select name="search_gender" id="search_gender" class="form-control col-sm-2" style="margin-left: 850px; margin-top: -38px;">
                <option value="" disabled selected>Gender</option>
                
                
                <?php
                    $gender_all = array("Male", "Female", "Others");
                    foreach ($gender_all as $key => $value):
                ?>
                        <option value="<?= $value; ?>" <?php echo (isset($_REQUEST['search_gender']) && $_REQUEST['search_gender'] == $value) ? 'selected' : '' ?>>
                            <?= $value ?>
                        </option>
                <?php endforeach; ?>
            </select><br>

            <input type="submit" value="Filter Records" class="form-control col-sm-3 btn btn-warning" style="margin-left: 660px;">

            <input type="hidden" name="type" value="search">
        </form>

        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
            
            <button type="submit" class="form-control col-sm-3 btn btn-outline-success" style="margin-left: 300px; margin-top: -54px;">
                Reset Filter
            </button>
            
            <input type="hidden" name="type" value="reset_filter">
        </form>

        <center>
            <?php

                function display_alert($session) {
                    unset($_SESSION['user']);
                    session_destroy();
                    ?>
                        <span class="alert <?php if($session == "Record added") echo "alert-success"; elseif($session == "Record updated") echo "alert-info"; elseif ($session == "Record deleted") echo "alert-danger" ?> alert-dismissible fade show" role="alert">
                            <strong><?php echo $session ;?> successfully</strong> 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </span>
                    <?php
                }

                if(isset($_SESSION['user'])) {
                    display_alert($_SESSION['user']);
                }
            ?>
            
            <a href="add.php">
                <button class="btn btn-outline-primary col-lg-10 mt-4">Add user</button>
            </a><br><br>

            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
            <?php
                $records = array("5", "10", "25", "50", "100");
            ?>
                <select name="page_length" id="page_length" class="form-control col-sm-2" style="margin-left: 600px;">
                    <?php
                        foreach ($records as $key => $value) {
                            ?>
                                <option value="<?php echo $value; ?>" <?php if($value == $num) echo "selected" ?>><?php echo $value." Records"; ?></option>
                            <?php
                        }
                    ?>
                </select>
                <input type="submit" value="Display" class="form-control col-sm-1 btn btn-info" style="margin-left: 960px; margin-top: -38px;">
            </form>

            <table class="table table-striped table-hover table-dark mt-4 col-lg-10 table-bordered" style="text-align: center;">
                <thead class="thead-light mx-auto">
                    <tr>
                        <th>Id</th>

                        <th>Image</th>

                        <th>
                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?sort=first_name&start=<?php echo $start; ?>&num=<?php echo $num ?>&type=<?php echo (isset($_GET['type']) && $_GET['type'] == 'DESC') ? 'ASC' : 'DESC' ?>">First Name</a>
                        </th>

                        <th>
                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?sort=last_name&start=<?php echo $start; ?>&num=<?php echo $num ?>&type=<?php echo (isset($_GET['type']) && $_GET['type'] == 'DESC') ? 'ASC' : 'DESC' ?>">Last Name</a>
                        </th>

                        <th>
                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?sort=email&start=<?php echo $start; ?>&num=<?php echo $num; ?>&type=<?php echo (isset($_GET['type']) && $_GET['type'] == 'DESC') ? 'ASC' : 'DESC' ?>">Email</a>
                        </th>

                        <th>
                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?sort=phone&start=<?php echo $start; ?>&num=<?php echo $num; ?>&type=<?php echo (isset($_GET['type']) && $_GET['type'] == 'DESC') ? 'ASC' : 'DESC' ?>">Contact No.</a>
                        </th>

                        <th>
                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?sort=created_at&start=<?php echo $start; ?>&num=<?php echo $num ?>&type=<?php echo (isset($_GET['type']) && $_GET['type'] == 'DESC') ? 'ASC' : 'DESC' ?>">Created At</a>
                        </th>
                        <!-- <th>Period passed since record creation</th> -->
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                        if($run) {
                            while($data = mysqli_fetch_assoc($run)) {
                                ?>
                                    <tr>
                                        <td><?= $data['id']; ?></td>
                                        <td>
                                            <img src="Accessories/images/<?php if($data['image']) echo $data['image']; ?>" style="width: 70px; height: 70px;" alt="Not uploaded">
                                        </td>
                                        <td><?= $data['first_name']; ?></td>
                                        <td><?= $data['last_name']; ?></td>
                                        <td><?= $data['email']; ?></td>
                                        <td><?= $data['phone']; ?></td>
                                        <?php
                                            $date = date_create($data['created_at']);
                                        ?>
                                        <td><?php echo $d = date_format($date, "d-F-Y g A") ?></td>
                                        <?php
                                            $curr_date = date_create(date("Y-m-d G:i:s"));
                                            $create_date = date_create($data['created_at']);

                                            $curr_month = date_format($curr_date, 'm')."<br>";
                                            $curr_day = date_format($curr_date, 'd')."<br>";
                                            $curr_hour = date_format($curr_date, 'H')."<br>";
                                            $curr_min = date_format($curr_date, 'i')."<br>";
                                            $curr_sec = date_format($curr_date, 's')."<br>";
                                            $create_month = date_format($create_date, 'm')."<br>";
                                            $create_day = date_format($create_date, 'd')."<br>";
                                            $create_hour = date_format($create_date, 'H')."<br>";
                                            $create_min = date_format($create_date, 'i')."<br>";
                                            $create_sec = date_format($create_date, 's')."<br>";

                                            $month = $curr_month - $create_month;
                                            $day = $curr_day - $create_day;
                                            $hour = $curr_hour - $create_hour;
                                            $min = $curr_min - $create_min;
                                            $sec = $curr_sec - $create_sec;
                                            $diff = array(array($month, "months"), array($day, "days"), array($hour, "hours"), array($min, "minutes"), array($sec, "seconds"));
                                        ?>
                                        <!-- <td>
                                            <?php 
                                                // foreach ($diff as $key => $value) {
                                                //     if ($value[0] != 0) {
                                                //         echo $value[0]." ".$value[1];
                                                //         break;
                                                //     }
                                                // }
                                            ?>
                                        </td> -->
                                        <td>
                                            <a href="edit.php?uid=<?= $data['id']; ?>">
                                                <button class="btn btn-outline-success fas fa-marker">Edit</button>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="Accessories/server.php?uid=<?= $data['id']; ?>&type=delete">
                                                <button class="btn btn-outline-danger far fa-trash-alt">Delete</button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                            }
                        }
                    ?>
                </tbody>
            </table>

            <ul class="pagination" style="margin-left: 45%;">
                <?php

                    $select = "SELECT COUNT(*) FROM `users`";
                    $run = mysqli_query($conn, $select);
                    $data = mysqli_fetch_assoc($run);

                    $count = $data['COUNT(*)'];

                    $page = ceil($count / $num);
                    $curr_start = $_REQUEST['start'];

                    for($i = 0; $i < $page; $i++) {
                        $start = $i * $num;
                        $j = $i + 1;
                        ?>
                            <li class="page-item">
                                <a href="<?php echo $_SERVER['PHP_SELF']; ?>?start=<?php echo $start; ?>&num=<?php echo $num; ?>" class="page-link">
                                    <?php 
                                        if ($start == $curr_start)
                                        { 
                                            echo "<u>".$j."</u>"; 
                                        }
                                        else 
                                        {
                                            echo $j;
                                        } 
                                    ?>
                                </a>
                            </li>
                        <?php
                    }
                ?>
            </ul>
        </center>
        <p style="text-align: right; color: red;">Showing <?php echo $num; ?> out of <?php echo $count; ?> records</p>
    </body>
</html>